#include <reg51.h>

sbit P0_4 = P0^4;
sbit P0_3 = P0^3;
sbit P0_2 = P0^2;
sbit P0_7 = P0^7;
sbit P0_6 = P0^6;
sbit P0_5 = P0^5;
sbit P2_2 = P2^2;
sbit P2_1 = P2^1;
sbit P2_0 = P2^0;
sbit P3_3 = P3^3;
sbit P0_1 = P0^1;
sbit P0_0 = P0^0;

char hasWon(char player);
char getGameStatus();
void resetGame();
void drawOutput();
void renderGame();
void gameOverAnimation(char winner);
void changePlayerAnimation(char player);
void update();
char checkGameStatus();
void selectNextField();

/* Das Spielfeld ist wie folgt aufgebaut:
 * 0 1 2
 * 3 4 5
 * 6 7 8
 */
char gameField[9];

// renderOutput ist die aktulle programmausgabe, welche �ber drawOutput() ausgegeben wird. (0 = beide LEDs aus, 1 = LED 1 an, 2 = LED 2 an, 3 = beide LEDs an)
char renderOutput[9];

// selectedField ist das aktuell ausgew�hlte Feld.
char selectedField;

// Der aktuell spielender Spieler.
char currentPlayer;

// Der zu malende Spieler (benutzt f�r Multiplexing)
char playerToRender = 1;

char buttonPressed = 0;
unsigned int buttonPressedTime = 0;

// lastBlinkTime is used to archive the blinking effect on the game fields currently selected field
unsigned long int lastBlinkTime;

char main()
{
    // Spiel zur�cksetzen
    resetGame();

    while (1)
    {
        // Spielstand ausgeben
        renderGame();

        // Spiel aktualisieren
        update();
    }
    
    return 0;
}

// selectNextField w�hlt das n�chste zur verf�gung stehende Feld aus.
void selectNextField()
{
    char i;

    // �berpr�fe die n�chsten 8 Felder (starte bei i = 1 damit nur die n�chsten Felder �berpr�ft werden)
    for (i = 1; i < 9; i++)
    {
        // Wenn das Feld auf dem Spielfeld frei ist,
        if (gameField[(selectedField + i) % 9] == 0)
        {
            // w�hle das Feld aus und brich ab
            selectedField = (selectedField + i) % 9;
            return;
        }
    }
}

// update aktualisiert das Spiel
void update()
{
    // Wenn der eingabe Button gedr�ckt ist,
    if (P3_3)
    {
        // erh�he buttonPressedTime damit wir schauen k�nnen wie lange der Button gedr�ckt ist
        buttonPressedTime++;

        // setze buttonPressed aus "wahr" damit wir schauen k�nnen ob wir auf den Tastendruck schon reagiert haben
        buttonPressed = 1;
    }

    // Wenn der Button losgelassen wird und wir auf den Tastendruck noch nicht ragiert haben
    if (!P3_3 && buttonPressed)
    {
        // W�hle das n�chste freie Feld aus
        selectNextField();

        // Setzte buttonPressed auf "falsch" und markiere damit das wir auf den Knopfdruck schon reagiert haben
        buttonPressed = 0;

        // Setzte buttonPressedTime auf 0 um f�r einen n�chsten Tastendruck alles vorzubereiten
        buttonPressedTime = 0;
    }

    // Wenn der knopf l�nger als 1000 Zyklen gr�ckt gehalten wurde
    if (buttonPressedTime > 1000)
    {
        // Setzte das aktuell ausgew�hlte Feld zu den aktuell ausgew�hlten Spieler
        gameField[selectedField] = currentPlayer;

        // �berpr�fe ob das ausw�hlen das Feldes das Spiel beendet hat
        if (!checkGameStatus())
        {
            // Wenn ja, breche das update up
            return;
        }

        // W�chsle den akuell ausgew�hlten Spieler
        currentPlayer = currentPlayer == 1 ? 2 : 1;

        // Spiele die Spieler-Wechsel-Animation ab
        changePlayerAnimation(currentPlayer);

        // W�hle das n�chste Feld aus
        selectNextField();

        // Setzte buttonPressed auf "falsch" und markiere damit das wir auf den Knopfdruck schon reagiert haben
        buttonPressed = 0;

        // Setzte buttonPressedTime auf 0 um f�r einen n�chsten Tastendruck alles vorzubereiten
        buttonPressedTime = 0;
    }
}

// hasWon gibt wahr zur�ck wenn das gegebene Spieler gewonnen hat
char hasWon(char player)
{
    char i, j;

    // Das das spielfeld 3 zeilen/splaten hat, f�hre die zeilen/splaten �berpr�fung 3 mal aus
    for (i = 0; i < 3; ++i)
    {
        // �berpr�fe die Zeile
        for (j = 0; j < 3; ++j)
        {
            // Wenn ein Feld in der Zeile nicht dem Spieler geh�rt, gebe zur n�chsten �berp�fung.
            if (gameField[i * 3 + j] != player)
            {
                break;
            }

            // Wenn dem Spieler alle Zeilen geh�ren, hat er gewonnen.
            if (j == 2)
            {
                return 1;
            }
        }

        // �berpr�fe die Splate
        for (j = 0; j < 3; ++j)
        {
            // Wenn ein Feld in der Splate nicht dem Spieler geh�rt, gebe zur n�chsten �berp�fung.
            if (gameField[i + j * 3] != player)
            {
                break;
            }

            // Wenn dem Spieler alle Spalten geh�ren, hat er gewonnen.
            if (j == 2)
            {
                return 1;
            }
        }
    }

    // �berpr�fe die 1. Diagonale
    if (gameField[0] == player && gameField[4] == player && gameField[8] == player)
        return 1;

    // �berpr�fe die 2. Diagonale
    if (gameField[2] == player && gameField[4] == player && gameField[6] == player)
        return 1;

    // Der spieler hat keinen der Tests bestanden, also hat er noch nicht gewonnen,
    return 0;
}

// getGameStatus gibt den aktuellen Spielstatus zur�ck (0 = Spiel ist noch nicht vorbeit, 1 = Spieler 1 hat gewonnen, 2 = Spieler 2 hat gewonnen, 3 = Unentschieden)
char getGameStatus()
{
    char i;

    // Wenn Spieler 1 gewonnen hat, gebe 1 zur�ck
    if (hasWon(1))
        return 1;

    // Wenn Spieler 2 gewonnen hat, gebe 2 zur�ck
    if (hasWon(2))
        return 2;

    // Wenn es freie Felder gibt
    for (i = 0; i < 9; ++i)
    {
        if (gameField[i] == 0)
        {
            // Gebe 0 zur�ck
            return 0;
        }
    }

    // Wenn es keine freien felder gibt, gebe 3 zur�ck
    return 3;
}

// resetGame setzte alle variablen auf ihre Ausgangswerte
void resetGame()
{
    char i;

    // Setze alle Spielfelder auf 0
    for (i = 0; i < 9; ++i)
    {
        gameField[i] = 0;
    }

    // Setze all Felder des renderOutput auf 0
    for (i = 0; i < 9; ++i)
    {
        renderOutput[i] = 0;
    }

    // Setzte das ausge�hlte Feld auf 4 (Mitte des Spielfeldes)
    selectedField = 4;

    // Setze lastBlinkTime auf 0
    lastBlinkTime = 0;

    // Setze den aktuellen Spieler auf Spieler 1
    currentPlayer = 1;

    // Set die buttonPressedTime auf 0
    buttonPressedTime = 0;

    // Set buttonPressed auf 0
    buttonPressed = 0;
}

// checkGameStatus �berpr�ft den Spielstatus und spielt die gameOverAnimation ab, setzt das Spiel zur�ck falls das Spiel vorbeit ist.
char checkGameStatus()
{ 
    char gameState;

    // Speiche den aktuellen SpielStatus in gameState
    gameState = getGameStatus();

    // Wenn das spiel noch am laufen ist
    if (gameState == 0)
    {
        // Gebe wahr zur�ck
        return 1;
    } 
       
    // Wenn das spiel vorbei ist, spiele die gameOverAnimation f�r den gameState ab.
    gameOverAnimation(gameState);

    // Setzte das spiel zur�ck
    resetGame();

    // Gebe falsch zur�ck, da das Spiel vorbei ist.
    return 0;
}

// drawOutput gibt den renderoutput auf dem Spielfeld aus
void drawOutput()
{
    // Schalte alle LEDs auf bevor wir den auszugebenen Spieler wechseln
    P0_4 = 0;
    P0_3 = 0;
    P0_2 = 0;
    P0_7 = 0;
    P0_6 = 0;
    P0_5 = 0;
    P2_2 = 0;
    P2_1 = 0;
    P2_0 = 0;

    // Wechsle den auszugebenden Spieler
    playerToRender = playerToRender == 1 ? 2 : 1;

    // Schalte den Transistor f�r den auszugebenden Spieler an und den anderen aus.
    P0_1 = playerToRender == 1;
    P0_0 = playerToRender == 2;

    // Schalte die zu den playerToRender geh�renden Spielfelder aus und die anderen aus.
    P0_4 = renderOutput[0] == playerToRender || renderOutput[0] == 3;
    P0_3 = renderOutput[1] == playerToRender || renderOutput[1] == 3;
    P0_2 = renderOutput[2] == playerToRender || renderOutput[2] == 3;

    P0_7 = renderOutput[3] == playerToRender || renderOutput[3] == 3;
    P0_6 = renderOutput[4] == playerToRender || renderOutput[4] == 3;
    P0_5 = renderOutput[5] == playerToRender || renderOutput[5] == 3;

    P2_2 = renderOutput[6] == playerToRender || renderOutput[6] == 3;
    P2_1 = renderOutput[7] == playerToRender || renderOutput[7] == 3;
    P2_0 = renderOutput[8] == playerToRender || renderOutput[8] == 3;
}

// renderGame berechnet die neuen Feldzuweisungen und zeigt sie an
void renderGame()
{
    char i;

    // erh�ht lastBlinkTime um 1
    lastBlinkTime++;

    // Wenn die lastBlinkTime > 100 ist, setzte sie auf 0
    if (lastBlinkTime > 1000)
    {
        lastBlinkTime = 0;
    }
    
    for (i = 0; i < 9; ++i)
    {
        // Wenn die lastBlinkTime < 500 ist und i das ausgew�hlte Feld ist, setze die ausgabe das aktuellen Feldes zu dem akuellen Spieler um den "Blink-Effekt" zu erzielen.
        if (i == selectedField && lastBlinkTime < 500)
        {
            renderOutput[i] = currentPlayer;
        }
        
        // Wenn das nicht der Fall ist, kopiere einfach den gameField Wert in den renderOutput
        else
        {
            renderOutput[i] = gameField[i];
        }
    }

    // Zeichne die Ausgabe
    drawOutput();
}

// gameOverAnimation spielt eine gameOverAnimation ab.
void gameOverAnimation(char winner)
{
	 char i, j;

    // Wiederhole die Animation 3 mal:
    for (i = 0; i < 3; ++i)
    {
        // Setzte den renderoutput zu dem Gewinner.
        for (j = 0; j < 9; ++j)
        {
            renderOutput[j] = winner;
        }

        // Warte
        for (j = 0; j < 5000; j++)
        {
            drawOutput();
        }

        // Setzte den renderoutput zu dem Spielfeld.
        for (j = 0; j < 9; ++j)
        { 
            renderOutput[j] = gameField[j];
        }

        // Warte
        for (j = 0; j < 5000; j++)
        {
            drawOutput();
        }
    }
}

// changePlayerAnimation spielt eine player-wechsel-animation ab (1 = Spieler 1, 2 = Spieler 2)
void changePlayerAnimation(char player)
{
	 int j;

    // Setzte den renderoutput zu dem Gewinner.
    for (j = 0; j < 9; ++j)
    {
        renderOutput[j] = player;
    }
        
    // Warte
    for (j = 0; j < 5000; j++)
    {
        drawOutput();
    }
}


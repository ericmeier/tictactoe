#include <at89x52.h>

// Game logic related functions
char hasWon(char player);
char getGameStatus();
void resetGame();
void drawOutput();
void renderGame();
void gameOverAnimation(char winner);
void changePlayerAnimation(char player);
void update();
char checkGameStatus();
void selectNextField();

/* The game field is positioned as followed:
 * 0 1 2
 * 3 4 5
 * 6 7 8
 */
char gameField[9];

// The output to been drawn to the game field. Same positioning system as the gameField. (0 = both LEDs off, 1 = LED 1 on, 2 = LED 2 on, 3 = both LEDs on)
char renderOutput[9];

// selectedField if the on the game field currently selected field.
char selectedField;

// The if of the currently selecting player.
char currentPlayer;

// The current player to render (used for multiplexing)
char playerToRender = 1;

char buttonPressed = 0;
unsigned int buttonPressedTime = 0;

// lastBlinkTime is used to archive the blinking effect on the game fields currently selected field
unsigned long int lastBlinkTime;

char main()
{
    // Spiel zurücksetzen
    resetGame();

    while (1)
    {
        // Spielstand ausgeben
        renderGame();

        // Spiel aktualisieren
        update();
    }
}

// selectNextField wählt das nächste zur verfügung stehende Feld aus.
void selectNextField()
{
    // Überprüfe die nächsten 8 Felder (starte bei i = 1 damit nur die nächsten Felder überprüft werden)
    for (char i = 1; i < 9; i++)
    {
        // Wenn das Feld auf dem Spielfeld frei ist,
        if (gameField[(selectedField + i) % 9] == 0)
        {
            // wähle das Feld aus und brich ab
            selectedField = (selectedField + i) % 9;
            return;
        }
    }
}

// update aktualisiert das Spiel
void update()
{
    // Wenn der eingabe Button gedrückt ist,
    if (P3_3)
    {
        // erhöhe buttonPressedTime damit wir schauen können wie lange der Button gedrückt ist
        buttonPressedTime++;

        // setze buttonPressed aus "wahr" damit wir schauen können ob wir auf den Tastendruck schon reagiert haben
        buttonPressed = 1;
    }

    // Wenn der Button losgelassen wird und wir auf den Tastendruck noch nicht ragiert haben
    if (!P3_3 && buttonPressed)
    {
        // Wähle das nächste freie Feld aus
        selectNextField();

        // Setzte buttonPressed auf "falsch" und markiere damit das wir auf den Knopfdruck schon reagiert haben
        buttonPressed = 0;

        // Setzte buttonPressedTime auf 0 um für einen nächsten Tastendruck alles vorzubereiten
        buttonPressedTime = 0;
    }

    // Wenn der knopf länger als 1000 Zyklen grückt gehalten wurde
    if (buttonPressedTime > 1000)
    {
        // Setzte das aktuell ausgewählte Feld zu den aktuell ausgewählten Spieler
        gameField[selectedField] = currentPlayer;

        // Überprüfe ob das auswählen das Feldes das Spiel beendet hat
        if (!checkGameStatus())
            // Wenn ja, breche das update up
            return;

        // Wächsle den akuell ausgewählten Spieler
        currentPlayer = currentPlayer == 1 ? 2 : 1;

        // Spiele die Spieler-Wechsel-Animation ab
        changePlayerAnimation(currentPlayer);

        // Wähle das nächste Feld aus
        selectNextField();

        // Setzte buttonPressed auf "falsch" und markiere damit das wir auf den Knopfdruck schon reagiert haben
        buttonPressed = 0;

        // Setzte buttonPressedTime auf 0 um für einen nächsten Tastendruck alles vorzubereiten
        buttonPressedTime = 0;
    }
}

// hasWon gibt wahr zurück wenn das gegebene Spieler gewonnen hat
char hasWon(char player)
{
    // Das das spielfeld 3 zeilen/splaten hat, führe die zeilen/splaten überprüfung 3 mal aus
    for (char i = 0; i < 3; ++i)
    {
        // Überprüfe die Zeile
        for (char j = 0; j < 3; ++j)
        {
            // Wenn ein Feld in der Zeile nicht dem Spieler gehört, gebe zur nächsten Überpüfung.
            if (gameField[i * 3 + j] != player)
                break;

            // Wenn dem Spieler alle Zeilen gehören, hat er gewonnen.
            if (j == 2)
                return 1;
        }

        // Überprüfe die Splate
        for (char j = 0; j < 3; ++j)
        {
            // Wenn ein Feld in der Splate nicht dem Spieler gehört, gebe zur nächsten Überpüfung.
            if (gameField[i + j * 3] != player)
                break;

            // Wenn dem Spieler alle Spalten gehören, hat er gewonnen.
            if (j == 2)
                return 1;
        }
    }

    // Überprüfe die 1. Diagonale
    if (gameField[0] == player && gameField[4] == player && gameField[8] == player)
        return 1;

    // Überprüfe die 2. Diagonale
    if (gameField[2] == player && gameField[4] == player && gameField[6] == player)
        return 1;

    // Der spieler hat keinen der Tests bestanden, also hat er noch nicht gewonnen,
    return 0;
}

// getGameStatus gibt den aktuellen Spielstatus zurück (0 = Spiel ist noch nicht vorbeit, 1 = Spieler 1 hat gewonnen, 2 = Spieler 2 hat gewonnen, 3 = Unentschieden)
char getGameStatus()
{
    // Wenn Spieler 1 gewonnen hat, gebe 1 zurück
    if (hasWon(1))
        return 1;

    // Wenn Spieler 2 gewonnen hat, gebe 2 zurück
    if (hasWon(2))
        return 2;

    // Wenn es freie Felder gibt
    for (int i = 0; i < 9; ++i)
        if (gameField[i] == 0)
            // Gebe 0 zurück
            return 0;

    // Wenn es keine freien felder gibt, gebe 3 zurück
    return 3;
}

// resetGame setzte alle variablen auf ihre Ausgangswerte
void resetGame()
{
    // Setze alle Spielfelder auf 0
    for (int i = 0; i < 9; ++i)
        gameField[i] = 0;

    // Setze all Felder des renderOutput auf 0
    for (int i = 0; i < 9; ++i)
        renderOutput[i] = 0;

    // Setzte das ausgeählte Feld auf 4 (Mitte des Spielfeldes)
    selectedField = 4;

    // Setze lastBlinkTime auf 0
    lastBlinkTime = 0;

    // Setze den aktuellen Spieler auf Spieler 1
    currentPlayer = 1;

    // Set die buttonPressedTime auf 0
    buttonPressedTime = 0;

    // Set buttonPressed auf 0
    buttonPressed = 0;
}

// checkGameStatus überprüft den Spielstatus und spielt die gameOverAnimation ab, setzt das Spiel zurück falls das Spiel vorbeit ist.
char checkGameStatus()
{
    // Speiche den aktuellen SpielStatus in gameState
    char gameState = getGameStatus();

    // Wenn das spiel noch am laufen ist
    if (gameState == 0)
        // Gebe wahr zurück
        return 1;

    // Wenn das spiel vorbei ist, spiele die gameOverAnimation für den gameState ab.
    gameOverAnimation(gameState);

    // Setzte das spiel zurück
    resetGame();

    // Gebe falsch zurück, da das Spiel vorbei ist.
    return 0;
}

// drawOutput gibt den renderoutput auf dem Spielfeld aus
void drawOutput()
{
    // Schalte alle LEDs auf bevor wir den auszugebenen Spieler wechseln
    P0_4 = 0;
    P0_3 = 0;
    P0_2 = 0;
    P0_7 = 0;
    P0_6 = 0;
    P0_5 = 0;
    P2_2 = 0;
    P2_1 = 0;
    P2_0 = 0;

    // Wechsle den auszugebenden Spieler
    playerToRender = playerToRender == 1 ? 2 : 1;

    // Schalte den Transistor für den auszugebenden Spieler an und den anderen aus.
    P0_1 = playerToRender == 1;
    P0_0 = playerToRender == 2;

    // Schalte die zu den playerToRender gehörenden Spielfelder aus und die anderen aus.
    P0_4 = renderOutput[0] == playerToRender || renderOutput[0] == 3;
    P0_3 = renderOutput[1] == playerToRender || renderOutput[1] == 3;
    P0_2 = renderOutput[2] == playerToRender || renderOutput[2] == 3;

    P0_7 = renderOutput[3] == playerToRender || renderOutput[3] == 3;
    P0_6 = renderOutput[4] == playerToRender || renderOutput[4] == 3;
    P0_5 = renderOutput[5] == playerToRender || renderOutput[5] == 3;

    P2_2 = renderOutput[6] == playerToRender || renderOutput[6] == 3;
    P2_1 = renderOutput[7] == playerToRender || renderOutput[7] == 3;
    P2_0 = renderOutput[8] == playerToRender || renderOutput[8] == 3;
}

// renderGame berechnet die neuen Feldzuweisungen und zeigt sie an
void renderGame()
{
    // erhöht lastBlinkTime um 1
    lastBlinkTime++;

    // Wenn die lastBlinkTime > 100 ist, setzte sie auf 0
    if (lastBlinkTime > 1000)
        lastBlinkTime = 0;

    for (char i = 0; i < 9; ++i)
    {
        // Wenn die lastBlinkTime < 500 ist und i das ausgewählte Feld ist, setze die ausgabe das aktuellen Feldes zu dem akuellen Spieler um den "Blink-Effekt" zu erzielen.
        if (i == selectedField && lastBlinkTime < 500)
            renderOutput[i] = currentPlayer;

        // Wenn das nicht der Fall ist, kopiere einfach den gameField Wert in den renderOutput
        else
            renderOutput[i] = gameField[i];
    }

    // Zeichne die Ausgabe
    drawOutput();
}

// gameOverAnimation spielt eine gameOverAnimation ab.
void gameOverAnimation(char winner)
{
    // Repeat the "blinking" animation 3 times:
    for (int i = 0; i < 3; ++i)
    {
        // set the render output to the winner.
        for (int j = 0; j < 9; ++j)
            renderOutput[j] = winner;

        // This is just like a delay loop, but since we multiplex we still have to update the screen.
        for (int j = 0; j < 5000; j++)
            drawOutput();

        // copy the game field into the render output
        for (int j = 0; j < 9; ++j)
            renderOutput[j] = gameField[j];

        // This is just like a delay loop, but since we multiplex we still have to update the screen.
        for (int j = 0; j < 5000; j++)
            drawOutput();
    }
}

// changePlayerAnimation plays a change player animation (1 = Player 1, 2 = Player 2)
void changePlayerAnimation(char player)
{
    // set the render output to the winner.
    for (int j = 0; j < 9; ++j)
        renderOutput[j] = player;

    // This is just like a delay loop, but since we multiplex we still have to update the screen.
    for (int j = 0; j < 5000; j++)
        drawOutput();
}

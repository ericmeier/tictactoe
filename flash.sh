#!/bin/bash

if [[ $UID != 0 ]]; then
    echo "Please run this script with sudo:"
    echo "sudo $0 $*"
    exit 1
fi

mkdir out
sdcc main.c -o out/
dfu-programmer at89c5131 flash out/main.ihx --force
rm -rf out
